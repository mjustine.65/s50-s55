import Banner from '../components/Banner';

export default function NotFound() {
	
	const data = {
		title: "ERROR 404",
		content: "Page not found",
		destination: "/",
		label: "Back to Home"
	}

	return (
		<Banner data={data}/>
	)
}