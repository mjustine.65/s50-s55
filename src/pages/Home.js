import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
// import CourseCard from '../components/CourseCard';
import Resources from '../components/Resources';



export default function Home() {

	let data = {
		title: "Zuit Coding Bootcamp",
		content: "Opportunies for everyone, everywhere",
		destination: "/courses",
		label: "Enroll Now!"

	}

    return (
        <Fragment>
			<Banner data={data}/>
			<Highlights/>
			{/*<CourseCard/>*/}
			<Resources/>
		</Fragment>
    );
};