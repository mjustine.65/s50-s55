import { Row, Accordion, Button } from 'react-bootstrap';

export default function Resources() {
    return (
		<Row className = "mt-3 mb-3">
			<Accordion defaultActiveKey="0">
				<Accordion.Item eventKey="0">
		        	<Accordion.Header>What is JavaScript?</Accordion.Header>
		        	<Accordion.Body>
				        Welcome to the MDN beginner's JavaScript course! In this article we will look at JavaScript from a high level, answering questions such as "What is it?" and "What can you do with it?", and making sure you are comfortable with JavaScript's purpose.
				        <br/><Button variant="outline-primary" href="https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/What_is_JavaScript#a_high-level_definition" className ="mt-2 mb-2">Learn more</Button>{' '}
		        	</Accordion.Body>
	        	</Accordion.Item>
	        	<Accordion.Item eventKey="1">
		        	<Accordion.Header>Express <br/>Fast, unopinionated, minimalist web framework for Node.js</Accordion.Header>
		        	<Accordion.Body>
				        Express is a minimal and flexible Node.js web application framework that provides a robust set of features for web and mobile applications.
				        <br/><Button variant="outline-primary" href="https://www.tutorialspoint.com/expressjs/index.htm" className ="mt-2 mb-2">Learn more</Button>{' '}
		        	</Accordion.Body>
	        	</Accordion.Item>
	        	<Accordion.Item eventKey="2">
		        	<Accordion.Header>Introduction To Node.Js</Accordion.Header>
		        	<Accordion.Body>
				        As an asynchronous event-driven JavaScript runtime, Node.js is designed to build scalable network applications. In the following "hello world" example, many connections can be handled concurrently. Upon each connection, the callback is fired, but if there is no work to be done, Node.js will sleep.
				        <br/><Button variant="outline-primary" href="https://nodejs.dev/en/learn/" className ="mt-2 mb-2">Learn more</Button>{' '}
		        	</Accordion.Body>
	        	</Accordion.Item>
	        	<Accordion.Item eventKey="3">
		        	<Accordion.Header>Tech 101: What Is React JS? By Scott Morris</Accordion.Header>
		        	<Accordion.Body>
				        React JS is a JavaScript library used in web development to build interactive elements on websites. But if you’re not familiar with JavaScript or JavaScript libraries, that’s not a helpful definition. So let’s take a step back and deal with those terms first.
				        <br/><Button variant="outline-primary" href="https://skillcrush.com/blog/what-is-react-js/#what" className ="mt-2 mb-2">Learn more</Button>{' '}
		        	</Accordion.Body>
	        	</Accordion.Item>
	        </Accordion>
		</Row>
    			
			
		
    )
}