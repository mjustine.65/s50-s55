import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
// import AppNavbar from './AppNavbar';

// Importing Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css'


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

/*const name = 'Marnel Justine Brobio'
const student = {
  firstname: "Lorem",
  lastname: "Ipsum"
}

function userName(user){
  return `${user.firstname} ${user.lastname}`
}

const element = <h1>Hello, {userName(student)}</h1>

root.render(element)*/